package com.zyz.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.zyz.entity.Apply;
import com.zyz.entity.User;
import com.zyz.utils.JdbcUtils;

public class ApplyDao {
	     
	Connection conn = null;

	PreparedStatement ps=null;
                                            
	ResultSet rs = null;
	/**锟斤拷雍锟斤拷锟斤拷锟斤拷锟�
	 * @param user
	 * @param friendId       
	 * @param friendName
	 */
	public void addApply(User user,int friendId, String friendName)  {

		

		try {
			conn = JdbcUtils.getConnection();

			String sql = "insert into apply(user_id,friend_id,user_name,friend_name,pass) values(?,?,?,?,?)";
			
			
			ps = conn.prepareStatement(sql);

			ps.setInt(1, user.getId());

			ps.setInt(2,friendId);
			
			ps.setString(3, user.getUsername());
			
			ps.setString(4, friendName);
			
			ps.setInt(5, 0);
			
			

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	/**
	 * 删锟斤拷锟斤拷锟斤拷锟斤拷锟�
	 * @param id
	 */
	
	public void delet(int id)  {

		

		try {
			conn = JdbcUtils.getConnection();

			String sql = "delete from apply where id=" + id;
			
			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	
	/**
	 * 同锟斤拷锟斤拷锟斤拷锟斤拷锟�
	 * 
	 */
	
	public void update(Apply apply)  {

		

		try {
			conn = JdbcUtils.getConnection();

			String sql = "update apply set pass='" + apply.getPass()
					+ "'where id='"
					+ apply.getId() + "'";

			
			ps = conn.prepareStatement(sql);

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}

	}
	
	/**
	 * 
	 * @param 锟斤拷询锟斤拷锟斤拷锟斤拷锟斤拷锟叫憋拷
	 * @return
	 */
	public List<Apply> findAllApply(User user )  {

		
		List<Apply> listApply = new ArrayList<Apply>();
		int pass = 0;

		try {
			conn = JdbcUtils.getConnection();

			String sql = "select * from apply where " +
						"friend_id ="+user.getId()+"" + 
						" and pass="+pass;
			
			ps = conn.prepareStatement(sql);

			rs=ps.executeQuery(sql);
			
			
			
			while (rs.next()) {
				
				Apply apply = new Apply();
				apply.setId(rs.getInt("id"));
				apply.setFriendId(rs.getInt("friend_id"));
				apply.setFriendName(rs.getString("friend_name"));
				apply.setUserId(rs.getInt("user_id"));
				apply.setUserName(rs.getString("user_name"));
				listApply.add(apply);

			}
			return listApply;

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {

			JdbcUtils.closeAll(conn, ps, rs);

		}
		

	}
	
	
	
	

}
