package com.zyz.entity;

public class Apply {
	
	private int id;
	
	private int userId;
	
	private int friendId;
	
	private String userName;
	
	private String friendName;
	
	private int pass;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getFriendId() {
		return friendId;
	}

	public void setFriendId(int friendId) {
		this.friendId = friendId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFriendName() {
		return friendName;
	}

	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}

	public int getPass() {
		return pass;
	}

	public void setPass(int pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return "Apply [id=" + id + ", userId=" + userId + ", friendId="
				+ friendId + ", userName=" + userName + ", friendName="
				+ friendName + ", pass=" + pass + "]";
	}
	
	

}
