package com.zyz.service;

import java.util.List;

import com.zyz.dao.UserDao;
import com.zyz.entity.User;

public class UserService {

	UserDao userDao = new UserDao();

	public User login(String username, String passwd) {

		boolean isLogin = false;

		return userDao.login(username, passwd);

	}

	public List<User> findAll(String userName,String sex,String place,String hobby) {

		return userDao.findAll(userName,sex,place,hobby);

	}

	public void addUser(User user) {

		userDao.addUser(user);

	}

	public void delet(int id) {

		userDao.delet(id);
	}

	public User findById(int id) {

		return userDao.findById(id);

	}

	public void update(User user) {

		userDao.update(user);

	}

	public List<User> findAllFriends(int userId) {
		
		return userDao.findAllFriends(userId);
	}
}
