package com.zyz.service;

import java.util.List;

import com.zyz.dao.ApplyDao;
import com.zyz.entity.Apply;
import com.zyz.entity.User;

public class ApplyService {
	
	ApplyDao applyDao = new ApplyDao();
	
	public void addApply(User user,int friendId, String friendName){
		applyDao.addApply(user, friendId, friendName);
	}
	
	
	
	
	public void delet(int id) {
		
		applyDao.delet(id);
	}
	
	
	
	public void update(Apply apply) {
		
		applyDao.update(apply);
	}
	
	
	
	public List<Apply> findAllApply(User user ) {
		return applyDao.findAllApply(user);
	}
	

}
