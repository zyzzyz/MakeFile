package com.zyz.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zyz.entity.User;
import com.zyz.service.ApplyService;
import com.zyz.service.FriendService;
import com.zyz.service.UserService;

public class UserServlet extends HttpServlet {
	
	UserService userService = new UserService();
	ApplyService applyService = new ApplyService();
	FriendService friendService = new FriendService();

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		String method = request.getParameter("method");
		
		if(method.equals("add")){
			addUser(request,response);
			
		}
		if(method.equals("find")){
			findById(request,response);
		}
		if(method.equals("update")){
			update(request,response);
		}
		
		if(method.equals("getAll")){
			getAllUser(request,response);
		}
		
		if(method.equals("addApply")){
			addApply(request,response);
		}
		
		if(method.equals("findFriendId")){
			findByFriendId(request,response);
		}
		

	}



	public void getAllUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String username=request.getParameter("username");
		
		String sex=request.getParameter("sex");
		String place=request.getParameter("place");
		String hobby=request.getParameter("hobby");
		
		List<User> listUser=userService.findAll( username, sex, place, hobby);
		
		request.setAttribute("listUser", listUser);
		request.getRequestDispatcher("/userList.jsp").forward(request, response);
		
		
		
		
	}


	public void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer id =Integer.parseInt(request.getParameter("id"));
		String username=request.getParameter("username");
		//String passwd=request.getParameter("passwd");
		String sex=request.getParameter("sex");
		int age=Integer.parseInt(request.getParameter("age"));
		String place=request.getParameter("place");
		String hobby=request.getParameter("hobby");
		
	    User user1=new User();
		user1.setId(id);
		user1.setUsername(username);
		//user1.setPasswd(passwd);
		user1.setSex(sex);
		user1.setAge(age);
		user1.setPlace(place);
		user1.setHobby(hobby);
		
		userService.update(user1);
		findById(request,response);
		
	}


	public void findById(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer id =Integer.parseInt(request.getParameter("id"));
		User user1=userService.findById(id);
		request.setAttribute("user", user1);
	
		request.getRequestDispatcher("/personInfo.jsp").forward(request, response);
		
	}
	
	public void findByFriendId(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer id =Integer.parseInt(request.getParameter("id"));
		User user1=userService.findById(id);
		request.setAttribute("user", user1);
	
		request.getRequestDispatcher("/friendInfo.jsp").forward(request, response);
		
	}


	private void addUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String username=request.getParameter("username");
		String passwd=request.getParameter("passwd");
		
		
		
		User user1=new User();
		
		user1.setUsername(username);
		user1.setPasswd(passwd);
		
		userService.addUser(user1);
		request.getRequestDispatcher("/index.jsp").forward(request, response);

		
	}
	
	public void addApply(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		User user=(User) session.getAttribute("user");
		
		request.setCharacterEncoding("UTF-8");
		
		int friendId =Integer.parseInt(request.getParameter("friendId"));
		String friendName =request.getParameter("friendName");
		friendName = new String(friendName.getBytes("ISO8859-1"),"UTF-8");
		applyService.addApply(user, friendId, friendName);
		getAllUser(request,response);
		
	}
	


	
}

