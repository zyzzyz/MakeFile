package com.zyz.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zyz.entity.User;
import com.zyz.service.UserService;



public class LoginServlet extends HttpServlet {

	UserService userService = new UserService();
	
	

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");

		String username = request.getParameter("username");

		String passwd = request.getParameter("passwd");

		User user = userService.login(username, passwd);

		if (null != user.getUsername()&& null != user.getPasswd()) {
			
			
			HttpSession session=request.getSession();
			session.setAttribute("user", user);

			request.getRequestDispatcher("/index.jsp").forward(request, response);
		} else {

			String message = "��¼ʧ�ܣ��û�����������";
			request.setAttribute("message", message);

			request.getRequestDispatcher("/login.jsp").forward(request,
					response);

		}

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}

}
