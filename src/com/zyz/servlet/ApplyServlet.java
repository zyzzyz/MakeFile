package com.zyz.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.zyz.entity.Apply;
import com.zyz.entity.User;
import com.zyz.service.ApplyService;
import com.zyz.service.FriendService;

public class ApplyServlet extends HttpServlet {

	ApplyService applyService = new ApplyService();
	
	FriendService friendService = new FriendService();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}
	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		String method = request.getParameter("method");
		
		if(method.equals("add")){
			addApply(request,response);
			
		}
		if(method.equals("find")){
			findAllApply(request,response);
		}
		
		
		if(method.equals("delete")){
			deleteApply(request,response);
		}
		
		if(method.equals("update")){
			agree(request,response);
		}

	}


	public void agree(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int friendId = Integer.parseInt(request.getParameter("friendId"));
		int id  = Integer.parseInt(request.getParameter("id"));
		Apply apply = new Apply();
		apply.setId(id);
		apply.setPass(1);
		applyService.update(apply);
		
		 HttpSession session = request.getSession();
		 User user=(User) session.getAttribute("user");
		 
		
		 friendService.addFriend(friendId, user.getId());
		 findAllApply(request,response);
		
		
	}


	public void deleteApply(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		int id  = Integer.parseInt(request.getParameter("id"));
		
		applyService.delet(id);
		findAllApply(request,response);
	
	
		
	}


	public void findAllApply(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
        HttpSession session = request.getSession();
		User user=(User) session.getAttribute("user");

		List<Apply> listApply =applyService.findAllApply(user);
		
		request.setAttribute("listApplay", listApply);

		
		request.getRequestDispatcher("/applyList.jsp").forward(request, response);

		
	}


	public void addApply(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		User user=(User) session.getAttribute("user");
		
		request.setCharacterEncoding("UTF-8");
		
		int friendId =Integer.parseInt(request.getParameter("friendId"));
		String friendName =request.getParameter("friendName");
		friendName = new String(friendName.getBytes("ISO8859-1"),"UTF-8");
		applyService.addApply(user, friendId, friendName);
		findAllApply(request,response);
	}
	

}
