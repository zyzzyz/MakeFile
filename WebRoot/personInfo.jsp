<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>修改个人信息</title>
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a href="#">首页</a>&nbsp;-&nbsp;<a
					href="#">个人中心</a>&nbsp;-</span>&nbsp;个人信息
			</div>
		</div>
		<div class="page ">
			<!-- 修改密码页面样式 -->
			<div class="bacen">
			<form action="${pageContext.request.contextPath }/userServlet?method=update" method="post">
  <input type="hidden" name="id" value="${user.id }"/>
 <table width="293" height="238" border="1" align="center" cellpadding="20" >
 
        <td width="298" height="232"><table align="center" >
    <tr>
      <th>用户名：</th>
      <td><input type="text" name="username" value="${user.username }"/></td>
    </tr>
    <tr>
      <th>性别：</th>
      <td><input type="radio" name="sex" value="男"  <c:if test="${user.sex=='男' }">checked="checked"</c:if> />
        男
        <input type="radio" name="sex" value="女"  <c:if test="${user.sex=='女' }">checked="checked"</c:if> />
        女 </td>
    </tr>
    <tr>
      <th>年龄：</th>
      <td><input type="text" name="age" value="${user.age }"/></td>
    </tr>
    <tr>
      <th>所在地：</th>
      <td><input type="text" name="place" value="${user.place }"/></td>
    </tr>
    <tr>
      <th>爱好：</th>
      <td><input type="text" name="hobby" value="${user.hobby }"/></td>
    </tr>
    <tr>
             <td height="50" colspan="2" align="center"><input type="submit" value="保存" />
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="reset" value="重置" /></td>
    </tr>
  </table>
  </td>
  </table>
</form>

			<!-- 修改密码页面样式end -->
		</div>
	</div>
</body>
<script type="text/javascript">
function checkpwd1(){
var user = document.getElementById('pwd1').value.trim();
 if (user.length >= 6 && user.length <= 12) {
  $("#pwd1").parent().find(".imga").show();
  $("#pwd1").parent().find(".imgb").hide();
 }else{
  $("#pwd1").parent().find(".imgb").show();
  $("#pwd1").parent().find(".imga").hide();
 };
}
function checkpwd2(){
var user = document.getElementById('pwd2').value.trim();
 if (user.length >= 6 && user.length <= 12) {
  $("#pwd2").parent().find(".imga").show();
  $("#pwd2").parent().find(".imgb").hide();
 }else{
  $("#pwd2").parent().find(".imgb").show();
  $("#pwd2").parent().find(".imga").hide();
 };
}
function checkpwd3(){
var user = document.getElementById('pwd3').value.trim();
var pwd = document.getElementById('pwd2').value.trim();
 if (user.length >= 6 && user.length <= 12 && user == pwd) {
  $("#pwd3").parent().find(".imga").show();
  $("#pwd3").parent().find(".imgb").hide();
 }else{
   $("#pwd3").parent().find(".imgb").show();
  $("#pwd3").parent().find(".imga").hide();
 };
}
</script>
</html>