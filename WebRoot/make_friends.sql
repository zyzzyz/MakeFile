/*
Navicat MySQL Data Transfer

Source Server         : 123456
Source Server Version : 50540
Source Host           : 127.0.0.1:3306
Source Database       : make_friends

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2017-06-06 18:30:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apply
-- ----------------------------
DROP TABLE IF EXISTS `apply`;
CREATE TABLE `apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `pass` int(11) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `friend_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apply
-- ----------------------------
INSERT INTO `apply` VALUES ('19', '3', '3', '1', '周', '周');
INSERT INTO `apply` VALUES ('22', '3', '2', '1', '周', '张');
INSERT INTO `apply` VALUES ('34', '3', '2', '1', '周', '张');
INSERT INTO `apply` VALUES ('38', '1', '2', '1', '李', '张');
INSERT INTO `apply` VALUES ('39', '1', '2', '1', '李', '张');
INSERT INTO `apply` VALUES ('40', '4', '1', '1', '王', '李');
INSERT INTO `apply` VALUES ('41', '4', '3', '1', '王', '周');
INSERT INTO `apply` VALUES ('43', '4', '1', '1', '王', '李');
INSERT INTO `apply` VALUES ('44', '1', '2', '1', '李', '张');
INSERT INTO `apply` VALUES ('46', '1', '3', '1', '李', '周');
INSERT INTO `apply` VALUES ('48', '4', '1', '1', '王', '李');
INSERT INTO `apply` VALUES ('49', '4', '3', '0', '王', '周');
INSERT INTO `apply` VALUES ('50', '4', '1', '1', '王', '李');
INSERT INTO `apply` VALUES ('51', '4', '4', '0', '王', '王');
INSERT INTO `apply` VALUES ('52', '2', '1', '1', '张三', '李四');
INSERT INTO `apply` VALUES ('54', '2', '3', '0', '张三', '周五');
INSERT INTO `apply` VALUES ('55', '2', '4', '1', '张三', '曹操');
INSERT INTO `apply` VALUES ('56', '2', '8', '1', '张三', '刘备');
INSERT INTO `apply` VALUES ('57', '10', '2', '0', '刘涛', '张三');
INSERT INTO `apply` VALUES ('58', '11', '2', '0', '张玉', '张三');
INSERT INTO `apply` VALUES ('60', '9', '2', '0', '诸葛亮', '张三');

-- ----------------------------
-- Table structure for friend
-- ----------------------------
DROP TABLE IF EXISTS `friend`;
CREATE TABLE `friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friend_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friend
-- ----------------------------
INSERT INTO `friend` VALUES ('10', '3', '4');
INSERT INTO `friend` VALUES ('12', '2', '1');
INSERT INTO `friend` VALUES ('13', '2', '3');
INSERT INTO `friend` VALUES ('15', '3', '1');
INSERT INTO `friend` VALUES ('16', '1', '4');
INSERT INTO `friend` VALUES ('17', '1', '2');
INSERT INTO `friend` VALUES ('18', '1', '4');
INSERT INTO `friend` VALUES ('19', '4', '2');
INSERT INTO `friend` VALUES ('20', '8', '2');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(11) DEFAULT NULL,
  `passwd` varchar(11) DEFAULT NULL,
  `sex` varchar(4) DEFAULT NULL,
  `age` int(4) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `hobby` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '李四', '1234', '男', '18', '南宁', '吃饭');
INSERT INTO `user` VALUES ('2', '张三', '1234', '女', '52', '北京', '喝酒');
INSERT INTO `user` VALUES ('3', '周五', '1234', '女', '25', '天津', '吃饭');
INSERT INTO `user` VALUES ('4', '曹操', '1234', '男', '23', '南宁', '喝酒');
INSERT INTO `user` VALUES ('8', '刘备', '1234', '男', '58', '南京', '猜码');
INSERT INTO `user` VALUES ('9', '诸葛亮', '1234', '男', '20', '南京', '吃饭');
INSERT INTO `user` VALUES ('10', '刘涛', '1234', '男', '30', '东京', '看书');
INSERT INTO `user` VALUES ('11', '张玉', '1234', '女', '40', '西宁', '睡觉');
